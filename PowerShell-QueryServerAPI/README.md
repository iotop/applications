# PowerShell-QueryAPI

The `PowerShell-QueryAPI.ps1` is a simple PowerShell script designed to parse the *LoRa Server* Application Programming Interface, or API. 

The API is available on the IoT Dunedin LoRa Server from the following address: `https://iot.op-bit.nz:8080/api`

## Script Requirements:

- An account on the LoRa Server
- The address of the LoRa Server, but this is currently hard-coded in the script
- Ability to execute PowerShell scripts, which is turned off by default in Microsoft Windows. There is a good [tutorial provided by Microsoft to enabled PowerShell scripts to run using Set-ExecutionPolicy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-6). Basically, you need to run the following command:

```
Set-ExecutionPolicy Unrestricted
```

Please note that this allows *any script to be executed* and may cause security issues. It is recommended that you disable after executing this script.

### Script Functionality

The `PowerShell-QueryAPI.ps1` is a very simple application that uses Microsoft Windows PowerShell to query to LoRa Server API to fetch information about applications. The resultant output, of found applications, is then printed to the console. The script can be extended to determine information about organisations, application, gateways and nodes.