"""

This script will REMOVE all data entry documents that were recorded within a second
of the previous data entry in the parent node document

It is configured to update all nodes in the node_data collection of the duniot_database

At line 22 you must first provide the details for the database and credentials for a user with read write access to the collection

"""


import datetime
import json
import base64
import csv
import os
import urllib.parse
from pymongo import MongoClient
from bson import json_util

USER = ''
PASS = ''
IP = ''
PORT = 

user = urllib.parse.quote_plus(USER)
passw = urllib.parse.quote_plus(PASS)
client = MongoClient('mongodb://%s:%s@%s:%s' % (user, passw, IP, PORT))
print("connected")
db = client.duniot_database 	# connect to the duniot_database
mqtt_collection = db.node_data	# connect to the node_data collection


all_data = json_util.loads(json_util.dumps(mqtt_collection.find()))

def remove_duplicate():
	previous_time = None
	previous_data = None
	for device in all_data:
		print(type(device["dataEntries"]))
		for data in device["dataEntries"]:
			print(type(data))
			if data["dbTime"] == previous_time and data["data"] == previous_data:
				# remove(data.id, mqtt_collection)
				print("Data: %s \t Time: %s" % (data["data"], data["dbTime"]))
			previous_time == data["dbTime"]
			previous_data == data["data"]
			
			
def test():
	for doc in all_data:
		seen = set()
		new_l = []
		for d in doc["dataEntries"]:
			t = tuple(sorted(d.items()))
			if t not in seen:
				seen.add(t)
				new_l.append(d)
		keep = remove_within_one_second(new_l)
		update(doc["_id"], keep)
	
	
def remove_within_one_second(data):
	previous_time = None
	previous_data = None
	keep = []
	for d in data:
		t = d["dbTime"]
		d_time = datetime.datetime(int(t[0:4]), int(t[5:7]), int(t[8:10]), int(t[11:13]), int(t[14:16]), int(t[17:19]))
		if previous_time:
			diff = (d_time - previous_time).seconds
			if diff > 1:
				keep.append(d)
		previous_data = d["data"]
		previous_time = d_time
	return keep


def update(obj_id, new_data_entries):
	mqtt_collection.update_one({"_id": obj_id}, {"$set": {"dataEntries": new_data_entries}})


test()