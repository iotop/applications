import datetime
import json
import base64
import csv
import os
import urllib.parse
from pymongo import MongoClient
from bson import json_util

USER = 'opiot'
PASS = 'P@ssw0rd'
IP = '10.118.26.11'
PORT = 27017

NODE_APPLICATION_NAMES = [] # leave blank for all applications
NODE_NAMES = [] # leave blank for all nodes

user = urllib.parse.quote_plus(USER)
passw = urllib.parse.quote_plus(PASS)
client = MongoClient('mongodb://%s:%s@%s:%s' % (user, passw, IP, PORT))
print("connected")
db = client.duniot_database 	# connect to the duniot_database
mqtt_collection = db.node_data	# connect to the node_data collection

all_data = json_util.loads(json_util.dumps(mqtt_collection.find()))

# filter for only data from specified node applications
if NODE_APPLICATION_NAMES:
	all_data = [dev for dev in all_data if dev['applicationName'] in NODE_APPLICATION_NAMES]

# filter for only data from specified node names
if NODE_NAMES:
	all_data = [dev for dev in all_data if dev['deviceName'] in NODE_NAMES]

def all():
	cwd = os.getcwd()
	data_folder_name = "\mqtt_node_export_" + get_formatted_time_str()
	os.mkdir(cwd + data_folder_name)
	for device in all_data:
		file_name = cwd + data_folder_name + "/" + device['devEUI'] + "_" + device['deviceName'] + "_" + device['applicationID'] + ".csv"
		with open(file_name, 'w', newline='') as csvfile:
			if device["dataEntries"]: # if list is not empty
				fieldnames = device["dataEntries"][0].keys()
				writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
				writer.writeheader()
				for entry in device["dataEntries"]:
					#row = entry
					row["data"] = base64ToString(row["data"])
					writer.writerow(row)
			else:
				print("No data entries for node %s" % device)

def base64ToString(encoded):
	return base64.b64decode(encoded).decode('utf-8')
	
def get_formatted_time_str():
    return datetime.datetime.now().strftime('%Y-%m-%d-%H%M%S')

all()