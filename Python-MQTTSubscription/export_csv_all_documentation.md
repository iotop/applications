# Export Script Documentation

This documentation provides a brief overview of the export_csv_all.py script.

## Overview
The script connects to the monogodb database (duniot_database) that holds all data received by the polytech gateway for lora devices.

Once connected all the data from the database is retrieved (the node_data collection).

That data is then decoded from base64 and used to create a csv file for each node object in the database.

Within the directory the script is run from a new directory is created to hold the output csv files.

## Requirements
Prior to running the script you will need to ensure you have python 3 installed.

Install pymongo, a package to allow you to interact with the monogo database.

Windows:

```python
pip install pymongo
```
Linux:

```python
pip3 install pymongo
```

## Run the script
Windows:

```python
cd <path-to-your-script>
```

```python
python export_csv_all.py
```


Linux:

```python
cd <path-to-your-script>
```

```python
python3 export_csv_all.py
```

## Naming conventions
Output directory:

"mqtt_node_export_" + date & time the export occured


CSV:

node device EUI + node name + node application ID

## Export data only for certain node names
To export data only for certain nodes you can add the node name/s as string to the NODE_NAMES list constant at line 16
```python
NODE_NAMES = ["my_node", "my_other_node"]
```

## Export data only for certain applications
To export data only for certain applications you can add the application name/s as string to the NODE_APPLICATION_NAMES list constant at line 15
```python
NODE_APPLICATION_NAMES = ["my_application", "my_other_application"]
```

## Connect to a different database
Adjust the four constants starting at line 10 to change the details of the database the script retrieves data from.

The user provided must have read access for the duniot_database.
```python
USER = 'username'
PASS = 'password'
IP = 'ip'
PORT = 12345
```

## Notes
- This script uses the urllib.parse.quote_plus() method to escape special characters when building the URI string to connect to the database. Without this connection will fail for certain user/passwords.
- Leaving the NODE_NAMES or NODE_APPLICATION_NAMES list empty will export all data in the database.
- The bson module is imported by pymongo, installing the bson module with pip will result in an error.
- To retrive node data in the form the gateway receives it uncomment line 47 and comment line 48. This avoids decoding the node data from base64.