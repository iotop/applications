# Export Script Documentation

This documentation provides a brief overview of the mqtt_minimal_listen_only.py script.
The script is intended to be used as a starting point or reference when creating other scripts that need to access the real time messages being published by the gateway.

## Overview
The script connects to the lora gateway and listens for incoming messages for all topics.
alternatively the user can run the script and pass the topic they will to listen for as a parameter.

When a message is received it is decoded from base64 and printed to the console along with the time the messgae was received by the local machine.

## Requirements
Prior to running the script you will need to ensure you have python 3 installed.

Install pymongo, a package to allow you to interact with the monogo database.

Windows:

```python
pip install paho-mqtt
```
Linux:

```python
pip3 install paho-mqtt
```

## Run the script

#Windows:

```python
cd <path-to-your-script>
```
Run the script and listen for all topics:
```python
python export_csv_all.py
```
Run the script and listen for a specific topic:
```python
python export_csv_all.py my_topic
```

#Linux:

```python
cd <path-to-your-script>
```
Run the script and listen for all topics:
```python
python3 export_csv_all.py
```
Run the script and listen for a specific topic:
```python
python3 export_csv_all.py my_topic
```

## Callbacks

The callback functions on_message() and on_connect() are defined and assigned to the mqtt client object. 
Everytime a message is receieved it will be passed to the on_message() function for processing. 

Everytime the mqtt client establishes a connection with the mqtt broker the on_connect() function is triggered. We subscribe to the desired topic within on_connect() to ensure that if connection is lost then restablished the same topic/s will be subscribed to.


## Decoding messages

All message payloads are encoded in base64, the base64_to_str() function decodes to human readable format. On occasion message payloads cannot be decoded, in that case the resultant UnicodeDecodeError exception will be caught and the script will continue.

Everything other than the message payload is in plain text e.g. the topic.
