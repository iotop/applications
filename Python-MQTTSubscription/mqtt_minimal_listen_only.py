import datetime
import json
import paho.mqtt.client as mqtt
import sys
import base64
import bson

# MQTT Settings 
MQTT_Broker = "iot.op-bit.nz"	# the web address that will publish the MQTT data
MQTT_Port = 1883		# web port to access MQTT_Broker
Keep_Alive_Interval = 45	# number of seconds to keep the connection alive between pings

MQTT_Topic = "application/#" # subscribe to all incoming messages that begin with 'application/' by default
if len(sys.argv) > 2 : MQTT_Topic = sys.argv[1] # if the user passed a parameter to the script, use the parameter as the topic instead.

# MQTT Methods:
# Subscribes to the specified topic (MQTT_Topic)
def on_connect(mqttc, mosq, obj, rc):
    mqttc.subscribe(MQTT_Topic, 0)
    print("Subscribed to topic: " + MQTT_Topic)


# When the message is received, it is processed and displayed
def on_message(mosq, obj, msg):
    time_received = get_formatted_time_str()
    json_message = json.loads(msg.payload.decode("utf-8"))

    print("\nMQTT Topic: " + msg.topic)
    # print the data and time received
    print("Time receieved: " + time_received)
    print("Data: " + base64_to_str(json_message["data"]))


# unused method
def on_subscribe(mosq, obj, mid, granted_qos):
    pass


# return current time as string in ISO-datetime format 8601DZ
def get_formatted_time_str():
    return datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')


# convert base64 encoded string to bytes then to string
def base64_to_str(data):
    try:
        decoded = base64.b64decode(data).decode('utf-8')
    except UnicodeDecodeError:
        decoded = "Payload %s could not be decoded from base64" % data
    return decoded


# Script Begins:
mqttc = mqtt.Client()
# Assign event callbacks
mqttc.on_message = on_message
mqttc.on_connect = on_connect
# Connect
print("Connecting...")
mqttc.connect(MQTT_Broker, int(MQTT_Port), int(Keep_Alive_Interval))
print("Connected")
# Continue the network loop
mqttc.loop_forever()

