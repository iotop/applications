// Set a new client variable with info on the MQTT server
// Change the host to correct MQTT server
// Also, set the clientID to anything (at the moment it is myclientID + random number)
var client = new Messaging.Client("iot.op-bit.nz", 1884, "myclientid_" + parseInt(Math.random() * 100, 10));

//declare variables
var inLow = 0;
var inHigh = 1023;
var outLow = 0;
var outHigh = 100;
var value = 0;
var once = 0;
var nodeValue = '0';

// Gets called if the websocket/MQTT connection gets disconnected for any reason
client.onConnectionLost = function (responseObject) {
    //Depending on your scenario you could implement a reconnect logic here
    alert("Disconnected" );
};

// Gets called whenever you receive a message for your subscriptions
client.onMessageArrived = function (message) {
    console.log("Got a message, it arrived!");   
  
    //putting message into JSONObject, parsing JSON, grabbing data and filling variables
    var JSONObject = JSON.parse(message.payloadString);  
    var appID = JSONObject.applicationID;   
    var appName = JSONObject.applicationName;   
    var nodeName1 = JSONObject.nodeName;    
    var dataDecode = JSONObject.data;
	
    //Decode the data using window.atob NOTE does not work on IE9 and below (but who cares about Internet Explorer anyway right?)
    var data = window.atob(dataDecode);   
    
	// Do calculaton on data to get a value between 0-100 for moisture gauge
	var calcValue = calc(data, inLow, inHigh, outLow, outHigh);
	var value = round2Fixed(calcValue);
    
    //Printing data out so you can see a history of it while subscribed
	var elem = document.getElementById("loading");
	elem.remove();
    $('#messages').append('<span>' + value + '</span><br/>');
	loading();
    
    if(once == 0)   //so text only gets added once
    {
        $('#appID').append('<p>'+ appID + '</p>');
        $('#appName').append('<p>' + appName +'</p>');
        $('#nodeName1').append('<p>'+ nodeName1 + '</p>');
        once++;
    }
    
    //update moisture gauge with data
    drawGauge(value);
};

//get the node value that is set for each node from html file
function getNodeValue(value)
{
nodeValue = value;
}


// Options for the initial connection
var options = {
    timeout: 3,
    // Gets Called if the connection has sucessfully been established
    onSuccess: function () {
        console.log("Connected");
		client.subscribe('application/4/node/'+nodeValue+'/rx', {qos: 2}); console.log('Subscribed');
		loading();
    },
    // Gets Called if the connection could not be established
    onFailure: function (message) {
        alert("Connection failed: " + message.errorMessage);
    }
};

function loading(){
	$('#messages').append('<p class="loader" id="loading"></p><br/>');
}


	
//function to decode base 64 strings
function calc (data, inLow, inHigh, outLow, outHigh) {
  return (data - inLow) * (outHigh - outLow) / (inHigh - inLow) + outLow;
}

//function to draw the moisture gauge
function drawGauge(value)  {
            $('#chartContainer').dxCircularGauge({
                scale: {
                        startValue: 0,
                        endValue: 100,
                        tickInterval: 10,
                        label: {
                            font: {
                                size:20
                            }
                        }
                        },
                rangeContainer: {
                    palette: 'pastel',
                            ranges: [
                                { color: 'orange', startValue: 0, endValue: 33 },
                                { color: 'green', startValue: 33, endValue: 66 },
                                { color: 'blue', startValue: 66, endValue: 100 },
                            ]
                },
                title: {
                    text: 'Moisture Level',
                    font: { size: 20 }
                },
                value: value
            });

        }

//function to fix the gauge value to 2 decimal places		
function round2Fixed(value) {
  value = +value;

  if (isNaN(value))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + 2) : 2)));

  // Shift back
  value = value.toString().split('e');
  return (+(value[0] + 'e' + (value[1] ? (+value[1] - 2) : -2))).toFixed(2);
}