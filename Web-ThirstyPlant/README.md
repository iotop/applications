# Web-ThirstyPlant

Example web application for the IoT Dunedin project group at Otago Polytechnic. The web application provides a friendly web-based user interface to map and parse data on moisture sensors in the Dunedin CDB area.
